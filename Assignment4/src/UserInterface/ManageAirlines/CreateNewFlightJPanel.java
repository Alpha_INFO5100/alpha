/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UserInterface.ManageAirlines;

import Business.AirLiner;
import Business.AirLinerDirectory;
import Business.Flight;
import Business.FlightDirectory;
import java.awt.CardLayout;
import java.awt.Color;
import java.awt.Component;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;

/**
 *

 */
public class CreateNewFlightJPanel extends javax.swing.JPanel {

    /**
     * Creates new form CreateNewFlightJPanel
     */
    private JPanel userProcessContainer;    
    private AirLinerDirectory ald;
    private AirLiner aln;
    private Flight fl;
    private FlightDirectory fld;
     private String schedule;
   
    
    
    public CreateNewFlightJPanel(JPanel userProcessContainer, FlightDirectory fld) {
        initComponents();
        this.userProcessContainer = userProcessContainer;
        this.ald = ald;
        this.aln = aln;
        this.fl = fl;
        this.fld = fld;
//    JRadioButton radioMorning = new JRadioButton("Morning");   
//    JRadioButton radioDay = new JRadioButton("Day");
//    JRadioButton radioEvening = new JRadioButton("Evening");
        
    }

 
      private void toManageAirliners(){
        CardLayout layout = (CardLayout) this.userProcessContainer.getLayout();
        this.userProcessContainer.remove(this);
        Component[] comps = this.userProcessContainer.getComponents();
        for(Component comp : comps){
            if (comp instanceof ManageAirlinersJPanel){
                ManageAirlinersJPanel panel = (ManageAirlinersJPanel) comp;
                 panel.populate1();
               panel.populate2();
               
            }
        }
        layout.previous(userProcessContainer);
    }
      
      private boolean airlinerNamePatternCorrect(){
          Pattern p = Pattern.compile("^[A-Za-z]+_[A-Za-z]+$");
          Matcher m = p.matcher(txtAirlinerName.getText());
          boolean b = m.matches();
          return b;          
      }
      private boolean flightNumberPatternCorrect(){
          Pattern p = Pattern.compile("^([A-Z]{1})+([A-Z]{1})+[0-9]{3}$");
          Matcher m = p.matcher(txtFlightNum.getText());
          boolean b = m.matches();
          return b;          
      }
      private boolean depAirportPatternCorrect(){
          Pattern p = Pattern.compile("^([A-Z]{3})$");
          Matcher m = p.matcher(txtDepAirport.getText());
          boolean b = m.matches();
          return b;          
      }
      private boolean arvAirportPatternCorrect(){
          Pattern p = Pattern.compile("^([A-Z]{3})$");
          Matcher m = p.matcher(txtArrivAirport.getText());
          boolean b = m.matches();
          return b;          
      }
      private boolean datePatternCorrect(){
          Pattern p = Pattern.compile("^([0-9]{2})+\\/([0-9]{2})+\\/[0-9]{4}$");
          Matcher m = p.matcher(txtDate.getText());
          boolean b = m.matches();
          return b;          
      }
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        buttonGroup1 = new javax.swing.ButtonGroup();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        txtAirlinerName = new javax.swing.JTextField();
        btnSave = new javax.swing.JButton();
        btnBack = new javax.swing.JButton();
        jLabel5 = new javax.swing.JLabel();
        txtFlightNum = new javax.swing.JTextField();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        txtNumOfSeats = new javax.swing.JTextField();
        txtDepAirport = new javax.swing.JTextField();
        txtDate = new javax.swing.JTextField();
        radioMorning = new javax.swing.JRadioButton();
        radioDay = new javax.swing.JRadioButton();
        radioEvening = new javax.swing.JRadioButton();
        jLabel10 = new javax.swing.JLabel();
        txtArrivAirport = new javax.swing.JTextField();

        setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel1.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N
        jLabel1.setText("Create New Flights");
        add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(107, 23, -1, -1));

        jLabel2.setText("Airliners");
        add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(117, 70, -1, -1));

        txtAirlinerName.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtAirlinerNameActionPerformed(evt);
            }
        });
        add(txtAirlinerName, new org.netbeans.lib.awtextra.AbsoluteConstraints(180, 70, 90, -1));

        btnSave.setText("Save");
        btnSave.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSaveActionPerformed(evt);
            }
        });
        add(btnSave, new org.netbeans.lib.awtextra.AbsoluteConstraints(250, 420, -1, -1));

        btnBack.setText("<<back");
        btnBack.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBackActionPerformed(evt);
            }
        });
        add(btnBack, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 420, -1, -1));

        jLabel5.setText("Flight Number");
        add(jLabel5, new org.netbeans.lib.awtextra.AbsoluteConstraints(87, 105, -1, -1));
        add(txtFlightNum, new org.netbeans.lib.awtextra.AbsoluteConstraints(180, 100, 90, -1));

        jLabel6.setText("Number of Seats");
        add(jLabel6, new org.netbeans.lib.awtextra.AbsoluteConstraints(70, 140, -1, -1));

        jLabel7.setText("Departure Airport");
        add(jLabel7, new org.netbeans.lib.awtextra.AbsoluteConstraints(60, 180, -1, -1));

        jLabel8.setText("Arrival Airport");
        add(jLabel8, new org.netbeans.lib.awtextra.AbsoluteConstraints(80, 210, -1, -1));

        jLabel9.setText("Date");
        add(jLabel9, new org.netbeans.lib.awtextra.AbsoluteConstraints(120, 250, -1, 10));
        add(txtNumOfSeats, new org.netbeans.lib.awtextra.AbsoluteConstraints(180, 140, 90, -1));
        add(txtDepAirport, new org.netbeans.lib.awtextra.AbsoluteConstraints(180, 180, 90, -1));
        add(txtDate, new org.netbeans.lib.awtextra.AbsoluteConstraints(180, 250, 90, -1));

        buttonGroup1.add(radioMorning);
        radioMorning.setText("Morning");
        radioMorning.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                radioMorningActionPerformed(evt);
            }
        });
        add(radioMorning, new org.netbeans.lib.awtextra.AbsoluteConstraints(180, 290, -1, -1));

        buttonGroup1.add(radioDay);
        radioDay.setText("Day");
        radioDay.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                radioDayActionPerformed(evt);
            }
        });
        add(radioDay, new org.netbeans.lib.awtextra.AbsoluteConstraints(180, 320, -1, -1));

        buttonGroup1.add(radioEvening);
        radioEvening.setText("Evening");
        radioEvening.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                radioEveningActionPerformed(evt);
            }
        });
        add(radioEvening, new org.netbeans.lib.awtextra.AbsoluteConstraints(180, 350, -1, -1));

        jLabel10.setText("Flight Schedule:");
        add(jLabel10, new org.netbeans.lib.awtextra.AbsoluteConstraints(70, 290, -1, -1));
        add(txtArrivAirport, new org.netbeans.lib.awtextra.AbsoluteConstraints(180, 210, 90, -1));
    }// </editor-fold>//GEN-END:initComponents

    private void btnBackActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBackActionPerformed
        // TODO add your handling code here:
        userProcessContainer.remove(this);
        CardLayout layout = (CardLayout) userProcessContainer.getLayout();
        layout.previous(userProcessContainer);
    }//GEN-LAST:event_btnBackActionPerformed

    private void btnSaveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSaveActionPerformed
        // TODO add your handling code here:
<<<<<<< HEAD
               String ALName = txtAirlinerName.getText();
               String FlightNumber = txtFlightNum.getText();               
               String NumberOfSeats = txtNumOfSeats.getText();               
=======
<<<<<<< HEAD
               String ALName = txtAirlinerName.getText();
               String FlightNumber = txtFlightNum.getText();               
               String NumberOfSeats = txtNumOfSeats.getText();               
=======
        Flight f = new Flight();
                String ALName = txtAirlinerName.getText();
                String FlightNumber = txtFlightNum.getText();               
                String NumberOfSeats = txtNumOfSeats.getText();               
>>>>>>> e92cb784625e7bd86561e1097831bfd251e18307
>>>>>>> private
               String DepartureAirport = txtDepAirport.getText();
               String ArrivalAirport = txtArrivAirport.getText();
               String Schedule = schedule;
               String Date = txtDate.getText();
//               String RadioMorning = radioMorning.getText();
//               String RadioDay = radioDay.getText();
//               String RadioEvening = radioEvening.getText();
//              private String Schedule;
//               String Schedule = buttonGroup1.getSelection().getActionCommand();
            txtAirlinerName.setBorder(javax.swing.BorderFactory.createSoftBevelBorder(javax.swing.border.BevelBorder.LOWERED,null,null, null, null));
        txtFlightNum.setBorder(javax.swing.BorderFactory.createSoftBevelBorder(javax.swing.border.BevelBorder.LOWERED,null,null, null, null));
        txtNumOfSeats.setBorder(javax.swing.BorderFactory.createSoftBevelBorder(javax.swing.border.BevelBorder.LOWERED,null,null, null, null));
       txtDepAirport.setBorder(javax.swing.BorderFactory.createSoftBevelBorder(javax.swing.border.BevelBorder.LOWERED,null,null, null, null));
       txtArrivAirport.setBorder(javax.swing.BorderFactory.createSoftBevelBorder(javax.swing.border.BevelBorder.LOWERED,null,null, null, null));
       txtDate.setBorder(javax.swing.BorderFactory.createSoftBevelBorder(javax.swing.border.BevelBorder.LOWERED,null,null, null, null));
        radioMorning.setForeground (Color.black);
        radioDay.setForeground (Color.black);
        radioEvening.setForeground (Color.black);  
               
                
        
          if(ALName.equals("")){
             txtAirlinerName.setBorder(javax.swing.BorderFactory.createSoftBevelBorder(javax.swing.border.BevelBorder.RAISED, java.awt.Color.red,new java.awt.Color(255, 0, 0), java.awt.Color.red, java.awt.Color.red));
            JOptionPane.showMessageDialog(null, "Airliner Name can't be empty");             
            return;
        }
          if(!airlinerNamePatternCorrect()){            
            txtAirlinerName.setBorder(javax.swing.BorderFactory.createSoftBevelBorder(javax.swing.border.BevelBorder.RAISED, java.awt.Color.red,new java.awt.Color(255, 0, 0), java.awt.Color.red, java.awt.Color.red));
            JOptionPane.showMessageDialog(null, "Airliner Name should be the form of Xxxx_Xxxx");
            
            return;
        }
          if(FlightNumber.equals("")){
             txtFlightNum.setBorder(javax.swing.BorderFactory.createSoftBevelBorder(javax.swing.border.BevelBorder.RAISED, java.awt.Color.red,new java.awt.Color(255, 0, 0), java.awt.Color.red, java.awt.Color.red));
            JOptionPane.showMessageDialog(null, "Flight Number can't be empty");             
            return;
        }
          if(!flightNumberPatternCorrect()){            
            txtFlightNum.setBorder(javax.swing.BorderFactory.createSoftBevelBorder(javax.swing.border.BevelBorder.RAISED, java.awt.Color.red,new java.awt.Color(255, 0, 0), java.awt.Color.red, java.awt.Color.red));
            JOptionPane.showMessageDialog(null, "Flight Number should be the form of XX000");
            
            return;
        }
          
          if(NumberOfSeats.equals("")){
             txtNumOfSeats.setBorder(javax.swing.BorderFactory.createSoftBevelBorder(javax.swing.border.BevelBorder.RAISED, java.awt.Color.red,new java.awt.Color(255, 0, 0), java.awt.Color.red, java.awt.Color.red));
            JOptionPane.showMessageDialog(null, "Number of Seats can't be empty");             
            return;
        }
            try{
                   Integer.parseInt(NumberOfSeats);
                   
            }catch(NumberFormatException e){ 
                JOptionPane.showMessageDialog(null, "Please enter a number");
                return;
            }
            if(Integer.parseInt(NumberOfSeats)> 150){
             txtNumOfSeats.setBorder(javax.swing.BorderFactory.createSoftBevelBorder(javax.swing.border.BevelBorder.RAISED, java.awt.Color.red,new java.awt.Color(255, 0, 0), java.awt.Color.red, java.awt.Color.red));
            JOptionPane.showMessageDialog(null, "Number of Seats can't be greater than 150");             
            return;
        }
            
          if(DepartureAirport.equals("")){
             txtDepAirport.setBorder(javax.swing.BorderFactory.createSoftBevelBorder(javax.swing.border.BevelBorder.RAISED, java.awt.Color.red,new java.awt.Color(255, 0, 0), java.awt.Color.red, java.awt.Color.red));
            JOptionPane.showMessageDialog(null, "Departure Airport can't be empty");             
            return;
        }
          if(!depAirportPatternCorrect()){            
            txtDepAirport.setBorder(javax.swing.BorderFactory.createSoftBevelBorder(javax.swing.border.BevelBorder.RAISED, java.awt.Color.red,new java.awt.Color(255, 0, 0), java.awt.Color.red, java.awt.Color.red));
            JOptionPane.showMessageDialog(null, "Departure airport name should be the form of XXX");
            return;
        }
          if(ArrivalAirport.equals("")){
             txtArrivAirport.setBorder(javax.swing.BorderFactory.createSoftBevelBorder(javax.swing.border.BevelBorder.RAISED, java.awt.Color.red,new java.awt.Color(255, 0, 0), java.awt.Color.red, java.awt.Color.red));
            JOptionPane.showMessageDialog(null, "Arrival airport can't be empty");             
            return;
        }
          if(!arvAirportPatternCorrect()){            
            txtArrivAirport.setBorder(javax.swing.BorderFactory.createSoftBevelBorder(javax.swing.border.BevelBorder.RAISED, java.awt.Color.red,new java.awt.Color(255, 0, 0), java.awt.Color.red, java.awt.Color.red));
            JOptionPane.showMessageDialog(null, "Arrival airport name should be the form of XXX");            
            return;
        }
          if(DepartureAirport.equals(ArrivalAirport)){
             txtArrivAirport.setBorder(javax.swing.BorderFactory.createSoftBevelBorder(javax.swing.border.BevelBorder.RAISED, java.awt.Color.red,new java.awt.Color(255, 0, 0), java.awt.Color.red, java.awt.Color.red));
             txtDepAirport.setBorder(javax.swing.BorderFactory.createSoftBevelBorder(javax.swing.border.BevelBorder.RAISED, java.awt.Color.red,new java.awt.Color(255, 0, 0), java.awt.Color.red, java.awt.Color.red));
             JOptionPane.showMessageDialog(null, "Departure Airport can't be the same as arrival airport");             
            return;
        }
          if(Date.equals("")){
             txtDate.setBorder(javax.swing.BorderFactory.createSoftBevelBorder(javax.swing.border.BevelBorder.RAISED, java.awt.Color.red,new java.awt.Color(255, 0, 0), java.awt.Color.red, java.awt.Color.red));
            JOptionPane.showMessageDialog(null, "Date can't be empty");             
            return;
        }
          if(!datePatternCorrect()){            
            txtDate.setBorder(javax.swing.BorderFactory.createSoftBevelBorder(javax.swing.border.BevelBorder.RAISED, java.awt.Color.red,new java.awt.Color(255, 0, 0), java.awt.Color.red, java.awt.Color.red));
            JOptionPane.showMessageDialog(null, "Date should be the form of mm/dd/yyyy");            
            return;
        }
           if(!radioMorning.isSelected() && !radioDay.isSelected() && !radioEvening.isSelected()){     
               radioMorning.setForeground (Color.red);
               radioDay.setForeground (Color.red);
               radioEvening.setForeground (Color.red);
            JOptionPane.showMessageDialog(null, "Please select a schedule!");            
            return;
        }

          
        
                   
        fld.addFlight(f);
        f.setALName(ALName);
        f.setFltNum(FlightNumber); 
        f.setSeatNum(Integer.parseInt(NumberOfSeats)); 
        f.setDeparAirport(DepartureAirport); 
        f.setArrivAirport(ArrivalAirport);
//        f.setSchedule(RadioMorning);
        f.setDate(Date);
        f.setSchedule(Schedule);
        
       
        
        JOptionPane.showMessageDialog(null,"Flight Added Successfully");
        
        toManageAirliners();
        txtAirlinerName.setText("");
        txtFlightNum.setText("");
        txtNumOfSeats.setText("");
        txtDepAirport.setText("");
        txtArrivAirport.setText("");
        txtDate.setText("");
        
        
        
    }//GEN-LAST:event_btnSaveActionPerformed

    private void txtAirlinerNameActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtAirlinerNameActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtAirlinerNameActionPerformed

    private void radioMorningActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_radioMorningActionPerformed
        
         schedule = "Morning";
        radioMorning.setSelected(false);
    }//GEN-LAST:event_radioMorningActionPerformed

    private void radioDayActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_radioDayActionPerformed
        schedule = "Day";
        radioDay.setSelected(false);
    }//GEN-LAST:event_radioDayActionPerformed

    private void radioEveningActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_radioEveningActionPerformed
         schedule = "Evening";
        radioEvening.setSelected(false);
    }//GEN-LAST:event_radioEveningActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnBack;
    private javax.swing.JButton btnSave;
    private javax.swing.ButtonGroup buttonGroup1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JRadioButton radioDay;
    private javax.swing.JRadioButton radioEvening;
    private javax.swing.JRadioButton radioMorning;
    private javax.swing.JTextField txtAirlinerName;
    private javax.swing.JTextField txtArrivAirport;
    private javax.swing.JTextField txtDate;
    private javax.swing.JTextField txtDepAirport;
    private javax.swing.JTextField txtFlightNum;
    private javax.swing.JTextField txtNumOfSeats;
    // End of variables declaration//GEN-END:variables

 
}
