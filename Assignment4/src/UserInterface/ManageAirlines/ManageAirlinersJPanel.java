/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UserInterface.ManageAirlines;

import Business.AirLiner;
import Business.AirLinerDirectory;
import Business.Flight;
import Business.FlightDirectory;
import java.awt.CardLayout;
import java.awt.Color;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author User
 */
public class ManageAirlinersJPanel extends javax.swing.JPanel {

    /**
     * Creates new form ManageAirliners
     */
    private JPanel userProcessContainer;
    private AirLinerDirectory ald;
    private AirLiner aln;
    private FlightDirectory fld;
    Flight f;
    
    public ManageAirlinersJPanel(JPanel userProcessContainer, AirLinerDirectory ald, FlightDirectory fld) {
        this.ald = ald;
        this.fld = fld;
        this.aln = aln;
        initComponents();
        populate1();
        populate2();
        this.userProcessContainer = userProcessContainer;
    }

  private boolean airlinerNamePatternCorrect(){
          Pattern p = Pattern.compile("^[A-Za-z]+_[A-Za-z]+$");
          Matcher m = p.matcher(txtALName.getText());
          boolean b = m.matches();
          return b;
          
      }
  
       private boolean airlinerAbbreviationPatternCorrect(){
          Pattern p = Pattern.compile("^[A-Z]{1}[A-Z]{1}$");
          Matcher m = p.matcher(txtAbb.getText());
          boolean b = m.matches();
          return b;
          
      }
    private boolean AirlinerNamePatternCorrect(){
          Pattern p = Pattern.compile("^[A-Za-z]+_[A-Za-z]+$");
          Matcher m = p.matcher(txtAlnName.getText());
          boolean b = m.matches();
          return b;
          
      }
      private boolean flightNumberPatternCorrect(){
          Pattern p = Pattern.compile("^([A-Z]{1})+([A-Z]{1})+[0-9]{3}$");
          Matcher m = p.matcher(txtFltNum.getText());
          boolean b = m.matches();
          return b;          
      }
      private boolean depAirportPatternCorrect(){
          Pattern p = Pattern.compile("^([A-Z]{3})$");
          Matcher m = p.matcher(txtDepAp.getText());
          boolean b = m.matches();
          return b;          
      }
      private boolean arvAirportPatternCorrect(){
          Pattern p = Pattern.compile("^([A-Z]{3})$");
          Matcher m = p.matcher(txtArrAp.getText());
          boolean b = m.matches();
          return b;          
      }
      private boolean datePatternCorrect(){
          Pattern p = Pattern.compile("^([0-9]{2})+\\/([0-9]{2})+\\/[0-9]{4}$");
          Matcher m = p.matcher(txtDate.getText());
          boolean b = m.matches();
          return b;          
      }
      private boolean schedulePatternCorrect(){
          Pattern p = Pattern.compile("^(Morning|Day|Evening)$");
          Matcher m = p.matcher(txtSchedule.getText());
          boolean b = m.matches();
          return b;          
      }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        btnCreateNewAirliners = new javax.swing.JButton();
        btnManageFlights = new javax.swing.JButton();
        jScrollPane2 = new javax.swing.JScrollPane();
        tblAirliners = new javax.swing.JTable();
        jLabel4 = new javax.swing.JLabel();
        btnConfirmAln = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        tblFlights = new javax.swing.JTable();
        jLabel2 = new javax.swing.JLabel();
        btnConfirmFlt = new javax.swing.JButton();
        btnDeleteFlt = new javax.swing.JButton();
        btnUpdateAln = new javax.swing.JButton();
        btnUpdateFlt = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        txtAbb = new javax.swing.JTextField();
        txtALName = new javax.swing.JTextField();
        btnDeleteAln = new javax.swing.JButton();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        txtNumSeats = new javax.swing.JTextField();
        txtAlnName = new javax.swing.JTextField();
        txtFltNum = new javax.swing.JTextField();
        txtDepAp = new javax.swing.JTextField();
        txtArrAp = new javax.swing.JTextField();
        txtDate = new javax.swing.JTextField();
        txtSchedule = new javax.swing.JTextField();

        setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        btnCreateNewAirliners.setText("Create New Airliners");
        btnCreateNewAirliners.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCreateNewAirlinersActionPerformed(evt);
            }
        });
        add(btnCreateNewAirliners, new org.netbeans.lib.awtextra.AbsoluteConstraints(600, 80, 150, -1));

        btnManageFlights.setText("Create New Flights");
        btnManageFlights.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnManageFlightsActionPerformed(evt);
            }
        });
        add(btnManageFlights, new org.netbeans.lib.awtextra.AbsoluteConstraints(800, 440, 130, -1));

        tblAirliners.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null},
                {null, null}
            },
            new String [] {
                "Airline Name", "Abbreviation"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tblAirliners.getTableHeader().setReorderingAllowed(false);
        jScrollPane2.setViewportView(tblAirliners);

        add(jScrollPane2, new org.netbeans.lib.awtextra.AbsoluteConstraints(80, 120, -1, 97));

        jLabel4.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N
        jLabel4.setText("List of Airliners");
        add(jLabel4, new org.netbeans.lib.awtextra.AbsoluteConstraints(241, 21, -1, -1));

        btnConfirmAln.setText("Confirm");
        btnConfirmAln.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnConfirmAlnActionPerformed(evt);
            }
        });
        add(btnConfirmAln, new org.netbeans.lib.awtextra.AbsoluteConstraints(600, 140, -1, -1));

        tblFlights.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null}
            },
            new String [] {
                "Airliner Name", "Flight Number", "Departure Airport", "Arrival Airport", "Date", "Schedule", "Number of Seats"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Integer.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tblFlights.getTableHeader().setReorderingAllowed(false);
        jScrollPane1.setViewportView(tblFlights);

        add(jScrollPane1, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 470, 740, 100));

        jLabel2.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N
        jLabel2.setText("List of Flights");
        add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(260, 330, -1, -1));

        btnConfirmFlt.setText("Confirm");
        btnConfirmFlt.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnConfirmFltActionPerformed(evt);
            }
        });
        add(btnConfirmFlt, new org.netbeans.lib.awtextra.AbsoluteConstraints(800, 500, -1, -1));

        btnDeleteFlt.setText("Delete");
        btnDeleteFlt.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDeleteFltActionPerformed(evt);
            }
        });
        add(btnDeleteFlt, new org.netbeans.lib.awtextra.AbsoluteConstraints(800, 530, -1, -1));

        btnUpdateAln.setText("Update");
        btnUpdateAln.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnUpdateAlnActionPerformed(evt);
            }
        });
        add(btnUpdateAln, new org.netbeans.lib.awtextra.AbsoluteConstraints(600, 110, -1, -1));

        btnUpdateFlt.setText("Update");
        btnUpdateFlt.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnUpdateFltActionPerformed(evt);
            }
        });
        add(btnUpdateFlt, new org.netbeans.lib.awtextra.AbsoluteConstraints(800, 470, -1, -1));

        jLabel1.setText("Abbreviation");
        add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(330, 80, -1, -1));

        jLabel3.setText("Airline Name");
        add(jLabel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(100, 80, -1, -1));
        add(txtAbb, new org.netbeans.lib.awtextra.AbsoluteConstraints(400, 80, 110, -1));
        add(txtALName, new org.netbeans.lib.awtextra.AbsoluteConstraints(170, 80, 140, -1));

        btnDeleteAln.setText("Delete");
        btnDeleteAln.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDeleteAlnActionPerformed(evt);
            }
        });
        add(btnDeleteAln, new org.netbeans.lib.awtextra.AbsoluteConstraints(600, 180, -1, -1));

        jLabel5.setText("Num of Seats");
        add(jLabel5, new org.netbeans.lib.awtextra.AbsoluteConstraints(670, 400, -1, -1));

        jLabel6.setText("Airliner Name");
        add(jLabel6, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 400, -1, -1));

        jLabel7.setText("Flight Number");
        add(jLabel7, new org.netbeans.lib.awtextra.AbsoluteConstraints(140, 400, -1, -1));

        jLabel8.setText("Dep Airport");
        add(jLabel8, new org.netbeans.lib.awtextra.AbsoluteConstraints(240, 400, -1, -1));

        jLabel9.setText("Arr Airport");
        add(jLabel9, new org.netbeans.lib.awtextra.AbsoluteConstraints(350, 400, -1, -1));

        jLabel10.setText("Date");
        add(jLabel10, new org.netbeans.lib.awtextra.AbsoluteConstraints(450, 400, -1, -1));

        jLabel11.setText("Schedule");
        add(jLabel11, new org.netbeans.lib.awtextra.AbsoluteConstraints(560, 400, -1, -1));
        add(txtNumSeats, new org.netbeans.lib.awtextra.AbsoluteConstraints(670, 420, 70, -1));
        add(txtAlnName, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 420, 70, -1));
        add(txtFltNum, new org.netbeans.lib.awtextra.AbsoluteConstraints(140, 420, 70, -1));
        add(txtDepAp, new org.netbeans.lib.awtextra.AbsoluteConstraints(240, 420, 70, -1));
        add(txtArrAp, new org.netbeans.lib.awtextra.AbsoluteConstraints(350, 420, 70, -1));
        add(txtDate, new org.netbeans.lib.awtextra.AbsoluteConstraints(450, 420, 70, -1));
        add(txtSchedule, new org.netbeans.lib.awtextra.AbsoluteConstraints(560, 420, 70, -1));
    }// </editor-fold>//GEN-END:initComponents

    private void btnManageFlightsActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnManageFlightsActionPerformed
        // TODO add your handling code here:
        CardLayout layout = (CardLayout) userProcessContainer.getLayout();
        userProcessContainer.add(new CreateNewFlightJPanel(userProcessContainer,fld));
        layout.next(userProcessContainer);
    }//GEN-LAST:event_btnManageFlightsActionPerformed

    private void btnCreateNewAirlinersActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCreateNewAirlinersActionPerformed
        // TODO add your handling code here:
//        CreateNewAirlinersJPanel panel = new CreateNewAirlinersJPanel(userProcessContainer);
//        userProcessContainer.add("CreateNewAirlinersJPanel", panel);
//        CardLayout layout = (CardLayout) userProcessContainer.getLayout();
//        layout.next(userProcessContainer);
        CardLayout layout = (CardLayout) userProcessContainer.getLayout();
        userProcessContainer.add(new CreateNewAirlinersJPanel(userProcessContainer,ald));
        layout.next(userProcessContainer);
        
    }//GEN-LAST:event_btnCreateNewAirlinersActionPerformed

    private void btnDeleteFltActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDeleteFltActionPerformed
        // TODO add your handling code here:
                                                 
        int selectedrow = tblFlights.getSelectedRow();
         if (selectedrow >= 0) {
            Flight fl = (Flight) tblFlights.getValueAt(selectedrow, 0);
            fld.deleteFlight(fl);
            JOptionPane.showMessageDialog(null, "Delete Successful");
            populate1();
        } else {
            JOptionPane.showMessageDialog(null, "Please select a row.");
        }
         populate2();

   
       
    
    }//GEN-LAST:event_btnDeleteFltActionPerformed

    private void btnConfirmAlnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnConfirmAlnActionPerformed
        // TODO add your handling code here:
         AirLiner al = new AirLiner();
       String ALName2 = txtALName.getText();
        String Abbreviation2 = txtAbb.getText();
        
        if(ALName2 == null || ALName2.equals("") || Abbreviation2 == null || Abbreviation2.equals("")){
            jLabel2.setForeground (Color.red);
            jLabel3.setForeground (Color.red);
             txtALName.setBorder(javax.swing.BorderFactory.createSoftBevelBorder(javax.swing.border.BevelBorder.RAISED, java.awt.Color.red,new java.awt.Color(255, 0, 0), java.awt.Color.red, java.awt.Color.red));
            txtAbb.setBorder(javax.swing.BorderFactory.createSoftBevelBorder(javax.swing.border.BevelBorder.RAISED, java.awt.Color.red,new java.awt.Color(255, 0, 0), java.awt.Color.red, java.awt.Color.red));
             JOptionPane.showMessageDialog(null, "Name or Abbreviation can't be empty!");           
            return;
            
        }
        if(!airlinerNamePatternCorrect()){
             jLabel2.setForeground (Color.red);
            txtALName.setBorder(javax.swing.BorderFactory.createSoftBevelBorder(javax.swing.border.BevelBorder.RAISED, java.awt.Color.red,new java.awt.Color(255, 0, 0), java.awt.Color.red, java.awt.Color.red));
            JOptionPane.showMessageDialog(null, "Airliner Name should be the form of Xxxx_Xxxx");
            
            return;
        }
        if(!airlinerAbbreviationPatternCorrect()){
             jLabel3.setForeground (Color.red);
            txtAbb.setBorder(javax.swing.BorderFactory.createSoftBevelBorder(javax.swing.border.BevelBorder.RAISED, java.awt.Color.red,new java.awt.Color(255, 0, 0), java.awt.Color.red, java.awt.Color.red));
            JOptionPane.showMessageDialog(null, "Abbreviation should be the form of XX in capitalization");
            
            return;
        }
         
         al.setALName(ALName2);
         al.setAbbreviation(Abbreviation2);
        ald.addAln(al);
       
        
         int selectedRow = tblAirliners.getSelectedRow();
        AirLiner aln = (AirLiner) tblAirliners.getValueAt(selectedRow, 0);
        ald.deleteAln(aln);
   
       
        
        JOptionPane.showMessageDialog(null, "Airliners updated successfully!");

        txtALName.setText("");
        txtAbb.setText("");
        populate1();

        
       
    }//GEN-LAST:event_btnConfirmAlnActionPerformed

    private void btnUpdateAlnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnUpdateAlnActionPerformed
        // TODO add your handling code here:
        int selectedrow = tblAirliners.getSelectedRow();

        if (selectedrow >= 0) {
            AirLiner p = (AirLiner) tblAirliners.getValueAt(selectedrow, 0);
            txtALName.setText(p.getALName());
            txtAbb.setText(p.getAbbreviation());
           
        } else {
            JOptionPane.showMessageDialog(null, "Please select a row.");
        }
    
        
        
//        txtALName.setEnabled(true);
//        txtAbb.setEnabled(true);
    
    }//GEN-LAST:event_btnUpdateAlnActionPerformed

    private void btnDeleteAlnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDeleteAlnActionPerformed

       int selectedrow = tblAirliners.getSelectedRow();
         if (selectedrow >= 0) {
            AirLiner aln = (AirLiner) tblAirliners.getValueAt(selectedrow, 0);
            ald.deleteAln(aln);
            JOptionPane.showMessageDialog(null, "Delete Successful");
            populate1();
        } else {
            JOptionPane.showMessageDialog(null, "Please select a row.");
        }
         populate2();
   
       
    
    }//GEN-LAST:event_btnDeleteAlnActionPerformed

    private void btnUpdateFltActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnUpdateFltActionPerformed
        // TODO add your handling code here:
           int selectedrow = tblFlights.getSelectedRow();
           

        if (selectedrow >= 0) {
            Flight p = (Flight) tblFlights.getValueAt(selectedrow, 0);
//            int seatNum = p.getSeatNum();
            txtAlnName.setText(p.getALName());
            txtFltNum.setText(p.getFltNum());
            txtArrAp.setText(p.getArrivAirport());
            txtDate.setText(p.getDate());
            txtDepAp.setText(p.getDeparAirport());
            txtFltNum.setText(p.getFltNum());
            txtNumSeats.setText(Integer.toString(p.getSeatNum()));
            txtSchedule.setText(p.getSchedule());
    
   
   
           
        } else {
            JOptionPane.showMessageDialog(null, "Please select a row.");
        }             
   
    }//GEN-LAST:event_btnUpdateFltActionPerformed

    private void btnConfirmFltActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnConfirmFltActionPerformed
        // TODO add your handling code here:
//                   Flight fl = new Flight();
        
                 String ALName = txtAlnName.getText();
                String FlightNumber = txtFltNum.getText();                                         
               String DepartureAirport = txtDepAp.getText();
               String ArrivalAirport = txtArrAp.getText();               
               String NumOfSeats = txtNumSeats.getText();
               String Schedule = txtSchedule.getText();
                String Date = txtDate.getText();
                      

            txtAlnName.setBorder(javax.swing.BorderFactory.createSoftBevelBorder(javax.swing.border.BevelBorder.LOWERED,null,null, null, null));
        txtFltNum.setBorder(javax.swing.BorderFactory.createSoftBevelBorder(javax.swing.border.BevelBorder.LOWERED,null,null, null, null));
        txtNumSeats.setBorder(javax.swing.BorderFactory.createSoftBevelBorder(javax.swing.border.BevelBorder.LOWERED,null,null, null, null));
       txtSchedule.setBorder(javax.swing.BorderFactory.createSoftBevelBorder(javax.swing.border.BevelBorder.LOWERED,null,null, null, null));
       txtArrAp.setBorder(javax.swing.BorderFactory.createSoftBevelBorder(javax.swing.border.BevelBorder.LOWERED,null,null, null, null));
       txtDepAp.setBorder(javax.swing.BorderFactory.createSoftBevelBorder(javax.swing.border.BevelBorder.LOWERED,null,null, null, null));
       txtDate.setBorder(javax.swing.BorderFactory.createSoftBevelBorder(javax.swing.border.BevelBorder.LOWERED,null,null, null, null));
       
               
                
        
          if(ALName.equals("")){
             txtAlnName.setBorder(javax.swing.BorderFactory.createSoftBevelBorder(javax.swing.border.BevelBorder.RAISED, java.awt.Color.red,new java.awt.Color(255, 0, 0), java.awt.Color.red, java.awt.Color.red));
            JOptionPane.showMessageDialog(null, "Airliner Name can't be empty");             
            return;
        }
          if(!AirlinerNamePatternCorrect()){            
            txtAlnName.setBorder(javax.swing.BorderFactory.createSoftBevelBorder(javax.swing.border.BevelBorder.RAISED, java.awt.Color.red,new java.awt.Color(255, 0, 0), java.awt.Color.red, java.awt.Color.red));
            JOptionPane.showMessageDialog(null, "Airliner Name should be the form of Xxxx_Xxxx");
            
            return;
        }
          if(FlightNumber.equals("")){
             txtFltNum.setBorder(javax.swing.BorderFactory.createSoftBevelBorder(javax.swing.border.BevelBorder.RAISED, java.awt.Color.red,new java.awt.Color(255, 0, 0), java.awt.Color.red, java.awt.Color.red));
            JOptionPane.showMessageDialog(null, "Flight Number can't be empty");             
            return;
        }
          if(!flightNumberPatternCorrect()){            
            txtFltNum.setBorder(javax.swing.BorderFactory.createSoftBevelBorder(javax.swing.border.BevelBorder.RAISED, java.awt.Color.red,new java.awt.Color(255, 0, 0), java.awt.Color.red, java.awt.Color.red));
            JOptionPane.showMessageDialog(null, "Flight Number should be the form of XX000");
            
            return;
        }
          
          if(NumOfSeats.equals("")){
             txtNumSeats.setBorder(javax.swing.BorderFactory.createSoftBevelBorder(javax.swing.border.BevelBorder.RAISED, java.awt.Color.red,new java.awt.Color(255, 0, 0), java.awt.Color.red, java.awt.Color.red));
            JOptionPane.showMessageDialog(null, "Number of Seats can't be empty");             
            return;
        }
            try{
                   Integer.parseInt(NumOfSeats);
                   
            }catch(NumberFormatException e){ 
                JOptionPane.showMessageDialog(null, "Please enter a number");
                return;
            }
            if(Integer.parseInt(NumOfSeats)> 150){
             txtNumSeats.setBorder(javax.swing.BorderFactory.createSoftBevelBorder(javax.swing.border.BevelBorder.RAISED, java.awt.Color.red,new java.awt.Color(255, 0, 0), java.awt.Color.red, java.awt.Color.red));
            JOptionPane.showMessageDialog(null, "Number of Seats can't be greater than 150");             
            return;
        }
            
          if(DepartureAirport.equals("")){
             txtDepAp.setBorder(javax.swing.BorderFactory.createSoftBevelBorder(javax.swing.border.BevelBorder.RAISED, java.awt.Color.red,new java.awt.Color(255, 0, 0), java.awt.Color.red, java.awt.Color.red));
            JOptionPane.showMessageDialog(null, "Departure Airport can't be empty");             
            return;
        }
          if(!depAirportPatternCorrect()){            
            txtDepAp.setBorder(javax.swing.BorderFactory.createSoftBevelBorder(javax.swing.border.BevelBorder.RAISED, java.awt.Color.red,new java.awt.Color(255, 0, 0), java.awt.Color.red, java.awt.Color.red));
            JOptionPane.showMessageDialog(null, "Departure airport name should be the form of XXX");
            return;
        }
          if(ArrivalAirport.equals("")){
             txtArrAp.setBorder(javax.swing.BorderFactory.createSoftBevelBorder(javax.swing.border.BevelBorder.RAISED, java.awt.Color.red,new java.awt.Color(255, 0, 0), java.awt.Color.red, java.awt.Color.red));
            JOptionPane.showMessageDialog(null, "Arrival airport can't be empty");             
            return;
        }
          if(!arvAirportPatternCorrect()){            
            txtArrAp.setBorder(javax.swing.BorderFactory.createSoftBevelBorder(javax.swing.border.BevelBorder.RAISED, java.awt.Color.red,new java.awt.Color(255, 0, 0), java.awt.Color.red, java.awt.Color.red));
            JOptionPane.showMessageDialog(null, "Arrival airport name should be the form of XXX");            
            return;
        }
          if(DepartureAirport.equals(ArrivalAirport)){
             txtArrAp.setBorder(javax.swing.BorderFactory.createSoftBevelBorder(javax.swing.border.BevelBorder.RAISED, java.awt.Color.red,new java.awt.Color(255, 0, 0), java.awt.Color.red, java.awt.Color.red));
             txtDepAp.setBorder(javax.swing.BorderFactory.createSoftBevelBorder(javax.swing.border.BevelBorder.RAISED, java.awt.Color.red,new java.awt.Color(255, 0, 0), java.awt.Color.red, java.awt.Color.red));
             JOptionPane.showMessageDialog(null, "Departure Airport can't be the same as arrival airport");             
            return;
        }
          if(Date.equals("")){
             txtDate.setBorder(javax.swing.BorderFactory.createSoftBevelBorder(javax.swing.border.BevelBorder.RAISED, java.awt.Color.red,new java.awt.Color(255, 0, 0), java.awt.Color.red, java.awt.Color.red));
            JOptionPane.showMessageDialog(null, "Date can't be empty");             
            return;
        }
          if(!datePatternCorrect()){            
            txtDate.setBorder(javax.swing.BorderFactory.createSoftBevelBorder(javax.swing.border.BevelBorder.RAISED, java.awt.Color.red,new java.awt.Color(255, 0, 0), java.awt.Color.red, java.awt.Color.red));
            JOptionPane.showMessageDialog(null, "Date should be the form of mm/dd/yyyy");            
            return;
        }
            if(Schedule.equals("")){
             txtSchedule.setBorder(javax.swing.BorderFactory.createSoftBevelBorder(javax.swing.border.BevelBorder.RAISED, java.awt.Color.red,new java.awt.Color(255, 0, 0), java.awt.Color.red, java.awt.Color.red));
            JOptionPane.showMessageDialog(null, "Schedule can't be empty");             
            return;
        }
            if(!schedulePatternCorrect()){
             txtSchedule.setBorder(javax.swing.BorderFactory.createSoftBevelBorder(javax.swing.border.BevelBorder.RAISED, java.awt.Color.red,new java.awt.Color(255, 0, 0), java.awt.Color.red, java.awt.Color.red));
            JOptionPane.showMessageDialog(null, "Schedule have to be Morning or Day or Evening");             
            return;
        }
          
      
        
       
        int selectedRow = tblFlights.getSelectedRow();
        Flight fl = (Flight) tblFlights.getValueAt(selectedRow, 0);
        
        fld.deleteFlight(fl);
        
        
         fld.addFlight(fl);         
        fl.setALName(ALName);
        fl.setFltNum(FlightNumber); 
        fl.setSeatNum(Integer.parseInt(NumOfSeats)); 
        fl.setDeparAirport(DepartureAirport); 
        fl.setArrivAirport(ArrivalAirport);
//        f.setSchedule(RadioMorning);
        fl.setDate(Date);
        fl.setSchedule(Schedule);
        
     JOptionPane.showMessageDialog(null, "Flight updated successfully!");

        populate2();
        
        txtAlnName.setText("");
        txtFltNum.setText("");
        txtDepAp.setText("");
        txtArrAp.setText("");
        txtNumSeats.setText("");
        txtSchedule.setText("");
        txtDate.setText("");
         
//         al.setALName(ALName2);
//         al.setAbbreviation(Abbreviation2);
//        ald.addAln(al); 
//         int selectedRow = tblAirliners.getSelectedRow();
//        AirLiner aln = (AirLiner) tblAirliners.getValueAt(selectedRow, 0);
//        ald.deleteAln(aln);
   
        
    }//GEN-LAST:event_btnConfirmFltActionPerformed
    
   public void populate1() {
        DefaultTableModel dtm1 = (DefaultTableModel)tblAirliners.getModel();
        dtm1.setRowCount(0);       
        for(AirLiner aln : ald.getAld())
        {
            Object row[] = new Object[3];
            row[0] = aln;           
            row[1] = aln.getAbbreviation();

            dtm1.addRow(row);
            
        }
    }
   public void populate2() {
        DefaultTableModel dtm2 = (DefaultTableModel) tblFlights.getModel();
        dtm2.setRowCount(0);

       for(Flight f : fld.getFld()) {
            Object row[] = new Object[7];
            row[0] = f;           
            row[1] = f.getFltNum();
            row[2] = f.getDeparAirport();
            row[3] = f.getArrivAirport();
            row[4] = f.getDate();
            row[5] = f.getSchedule();
            row[6] = f.getSeatNum();
            

            dtm2.addRow(row);
            
        }
    }
    

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnConfirmAln;
    private javax.swing.JButton btnConfirmFlt;
    private javax.swing.JButton btnCreateNewAirliners;
    private javax.swing.JButton btnDeleteAln;
    private javax.swing.JButton btnDeleteFlt;
    private javax.swing.JButton btnManageFlights;
    private javax.swing.JButton btnUpdateAln;
    private javax.swing.JButton btnUpdateFlt;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JTable tblAirliners;
    private javax.swing.JTable tblFlights;
    private javax.swing.JTextField txtALName;
    private javax.swing.JTextField txtAbb;
    private javax.swing.JTextField txtAlnName;
    private javax.swing.JTextField txtArrAp;
    private javax.swing.JTextField txtDate;
    private javax.swing.JTextField txtDepAp;
    private javax.swing.JTextField txtFltNum;
    private javax.swing.JTextField txtNumSeats;
    private javax.swing.JTextField txtSchedule;
    // End of variables declaration//GEN-END:variables
}
