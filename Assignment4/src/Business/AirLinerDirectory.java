package Business;
import java.util.ArrayList;


public class AirLinerDirectory {
    
    
    
    private ArrayList<AirLiner> ald;

    public AirLinerDirectory() {
        this.ald = new ArrayList<AirLiner>();
    }

     public AirLinerDirectory(ArrayList<AirLiner> ald) {
        this.ald = ald;
    }
    
    public ArrayList<AirLiner> getAld() {
        return this.ald;
    }

    public void setAld(ArrayList<AirLiner> ald) {
        this.ald = ald;
    }
    public AirLiner addAln(AirLiner al){
        //updateTime = new Date();
//        AirLiner aln = new AirLiner();
        ald.add(al);
        return al;
    }
    public void deleteAln(AirLiner aln)
    {
        //updateTime = new Date();
        ald.remove(aln);
    }    

}