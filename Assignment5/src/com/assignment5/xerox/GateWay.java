/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.assignment5.xerox;

import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.assignment5.entities.Customer;
import com.assignment5.entities.Item;
import com.assignment5.entities.Product;
import com.assignment5.entities.SalesPerson;

/**
 *
 * @author kasai
 */
public class GateWay {

	public static void main(String args[]) throws IOException {
		List<Product> products = new ArrayList<>();
		List<SalesPerson> salers = new ArrayList<>();
		List<Customer> customers = new ArrayList<>();

		DataGenerator generator = DataGenerator.getInstance();
		DataReader productReader = new DataReader(generator.getProductCataloguePath());
		String[] prodRow;
		while ((prodRow = productReader.getNextRow()) != null) {
			products.add(new Product(Integer.valueOf(prodRow[0]), Integer.valueOf(prodRow[1]),
					Integer.valueOf(prodRow[2]), Integer.valueOf(prodRow[3])));
		}
		int totalProfit = 0;
		Map<Integer, Integer> productTotals = new HashMap<>();
		Map<Integer, Integer> productQuantities = new HashMap<>();
		Map<Integer, Integer> productQuantitiesAbove = new HashMap<>();
		Map<Integer, Integer> bestCustomers = new HashMap<>();
		Map<Integer, Integer> bestSalers = new HashMap<>();
		DataReader orderReader = new DataReader(generator.getOrderFilePath());
		String[] orderRow;
		while ((orderRow = orderReader.getNextRow()) != null) {
			int productId = Integer.valueOf(orderRow[2]);
			int productQuantity = Integer.valueOf(orderRow[3]);
			int productSalePrice = Integer.valueOf(orderRow[6]);
			totalProfit += productQuantity * (productSalePrice - products.get(productId).getTargetPrice());
			if (productTotals.containsKey(productId)) {
				productTotals.replace(productId, productTotals.get(productId) + productQuantity * productSalePrice);
				productQuantities.replace(productId, productQuantities.get(productId) + productQuantity);
			} else {
				productTotals.put(productId, productQuantity * productSalePrice);
				productQuantities.put(productId, productQuantity);
			}
			if (productSalePrice > products.get(productId).getTargetPrice()) {
				if (productQuantitiesAbove.containsKey(productId)) {
					productQuantitiesAbove.replace(productId, productQuantitiesAbove.get(productId) + productQuantity);
				} else {
					productQuantitiesAbove.put(productId, productQuantity);
				}
			}
			Item item = new Item(Integer.valueOf(orderRow[1]), productId, productSalePrice, productQuantity);
			SalesPerson saler = new SalesPerson(Integer.valueOf(orderRow[4]));
			Customer customer = new Customer(Integer.valueOf(orderRow[5]));
			if (!salers.contains(saler))
				salers.add(saler);
			if (!customers.contains(customer))
				customers.add(customer);
			if (bestCustomers.containsKey(customer.getId())) {
				bestCustomers.replace(customer.getId(), bestCustomers.get(customer.getId()) + Math.abs(productSalePrice - products.get(productId).getTargetPrice()));
			} else {
				bestCustomers.put(customer.getId(), Math.abs(productSalePrice - products.get(productId).getTargetPrice()));
			}

			if (bestSalers.containsKey(saler.getId())) {
				bestSalers.replace(saler.getId(), bestSalers.get(saler.getId()) + productQuantity * (productSalePrice - products.get(productId).getTargetPrice()));
			} else {
				bestSalers.put(saler.getId(), productQuantity * (productSalePrice - products.get(productId).getTargetPrice()));
			}
		}
		
		Map<Integer, Double> productAverages = new HashMap<>();
		Map<Integer, Double> productDifferences = new HashMap<>();
		for (Integer productId : productTotals.keySet()) {
			double average = (double) productTotals.get(productId) / productQuantities.get(productId);
			productAverages.put(productId, average);
			productDifferences.put(productId, average - products.get(productId).getTargetPrice());
		}
		List<Map.Entry<Integer, Double>> list = new ArrayList<>(productDifferences.entrySet());
		Collections.sort(list, new Comparator<Map.Entry<Integer, Double>>() {

                        @Override
			public int compare(Map.Entry<Integer, Double> o1, Map.Entry<Integer, Double> o2) {
				if (o1.getValue() > o2.getValue())
					return -1;
				else if (o1.getValue() < o2.getValue())
					return 1;
				else {
					return 0;
				}
			}

		});
		DecimalFormat formater = new DecimalFormat("0.##");
		System.out.println("Product-Id, Target-Price, Average-Sale-Price, Difference");
		for (Map.Entry<Integer, Double> entry : list) {
			System.out.println(entry.getKey() + ", " + products.get(entry.getKey()).getTargetPrice() + ", "
					+ formater.format(productAverages.get(entry.getKey())) + ", "
					+ formater.format(entry.getValue()));
		}
		System.out.println("________________________________________________________________________________");
		System.out.println("Our top 3 best negotiated products (meaning products that sell above target)");
		Integer product = null;
		for (int j = 0; j < 3; j++) {
			if (product != null)
				productQuantitiesAbove.remove(product);
			product = null;
			for (Integer p : productQuantitiesAbove.keySet()) {
				if (product == null)
					product = p;
				else {
					if (productQuantitiesAbove.get(p) > productQuantitiesAbove.get(product))
						product = p;
				}
			}
			System.out.println((j + 1) + ". Product " + product + ", sale quantity " + productQuantitiesAbove.get(product));
		}
		System.out.println("________________________________________________________________________________");
		System.out.println("Our 3 best customers (customers who buy about target price)");
		Integer customer = null;
		for (int j = 0; j < 3; j++) {
			if (customer != null)
				bestCustomers.remove(customer);
			customer = null;
			for (Integer c : bestCustomers.keySet()) {
				if (customer == null)
					customer = c;
				else {
					if (bestCustomers.get(c) < bestCustomers.get(customer))
						customer = c;
				}
			}
			System.out.println((j + 1) + ". Customer " + customer + " about target $" + bestCustomers.get(customer));
		}
		System.out.println("________________________________________________________________________________");
		System.out.println("Our top 3 best sales people (sell higher that target)");
		Integer saler = null;
		for (int j = 0; j < 3; j++) {
			if (saler != null)
				bestSalers.remove(saler);
			saler = null;
			for (Integer s : bestSalers.keySet()) {
				if (saler == null)
					saler = s;
				else {
					if (bestSalers.get(s) > bestSalers.get(saler))
						saler = s;
				}
			}
			System.out.println((j + 1) + ". Sale person " + saler + " above target $" + bestSalers.get(saler));
		}
		System.out.println("________________________________________________________________________________");
		System.out.println("Our total revenue for the year that is above expected target");
		System.out.println("Total revenue: $" + totalProfit);
		System.out.println("________________________________________________________________________________");
		System.out.println("Modified target price");
		List<Product> modifiedProducts = new ArrayList<>();
		for (Product p : products) {
			modifiedProducts.add(new Product(p.getId(), p.getMinPrice(), p.getMaxPrice(), (int) Math.round(productAverages.get(p.getId()))));
		}
		Map<Integer, Double> productModifiedDifferences = new HashMap<>();
		for (Integer productId : productTotals.keySet()) {
			productModifiedDifferences.put(productId, productAverages.get(productId) - modifiedProducts.get(productId).getTargetPrice());
		}
		List<Map.Entry<Integer, Double>> list2 = new ArrayList<>(productModifiedDifferences.entrySet());
		Collections.sort(list2, new Comparator<Map.Entry<Integer, Double>>() {

                        @Override
			public int compare(Map.Entry<Integer, Double> o1, Map.Entry<Integer, Double> o2) {
				if (o1.getValue() > o2.getValue())
					return -1;
				else if (o1.getValue() < o2.getValue())
					return 1;
				else {
					return 0;
				}
			}

		});
		System.out.println("Product-Id, Target-Price, Average-Sale-Price, Difference");
		for (Map.Entry<Integer, Double> entry : list2) {
			System.out.println(entry.getKey() + ", " + modifiedProducts.get(entry.getKey()).getTargetPrice() + ", "
					+ formater.format(productAverages.get(entry.getKey())) + ", "
					+ formater.format(entry.getValue()));
		}
	}

	public static void printRow(String[] row) {
		for (String row1 : row) {
			System.out.print(row1 + ", ");
		}
		System.out.println("");
	}

}
