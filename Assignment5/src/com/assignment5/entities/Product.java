/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.assignment5.entities;

/**
 *
 * @author kasai
 */
public class Product {
    private int id;
    private int minPrice;
    private int maxPrice;
    private int targetPrice;
    
    public Product(int id, int minPrice, int maxPrice, int targetPrice) {
        this.id = id;
        this.minPrice = minPrice;
        this.maxPrice = maxPrice;
        this.targetPrice = targetPrice;
    }
    
    public int getId() {
        return this.id;
    }
    
    public int getMinPrice() {
        return this.minPrice;
    }
    
    public int getMaxPrice() {
        return this.maxPrice;
    }
    
    public int getTargetPrice() {
        return this.targetPrice;
    }
}
