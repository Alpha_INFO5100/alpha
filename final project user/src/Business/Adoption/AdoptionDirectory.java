/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Adoption;

import Business.Volunteer.*;
import Business.Animals.*;
import Business.UserAccount.*;
import Business.Employee.Employee;
import Business.Role.Role;
import java.util.ArrayList;

/**
 *
 * @author yibin
 */
public class AdoptionDirectory {
      private ArrayList<Adoption> adoptionlist;

    public AdoptionDirectory() {
        adoptionlist = new ArrayList();
    }

    public ArrayList<Adoption> getAccountList() {
        return adoptionlist;
    }

    public void setAccountList(ArrayList<Adoption> animallist) {
        this.adoptionlist = animallist;
    }

    public Adoption addAccount(){
        Adoption adoption = new Adoption();
        
        adoptionlist.add(adoption);
        return adoption;
    }
    
    public void deleteAccount(Adoption adoption){
        adoptionlist.remove(adoption);
    }
    
    public Adoption searchAccount (String name){
        for(Adoption adoption:adoptionlist){
            if(adoption.getName().equals(name)){
                return adoption;
            }
        }
        return null;
    }
    
    
}
