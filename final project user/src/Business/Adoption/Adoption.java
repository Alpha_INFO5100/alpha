/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Adoption;

import Business.Volunteer.*;
import Business.Animals.*;
import Business.UserAccount.*;
import Business.Employee.Employee;
import Business.Role.Role;
import Business.WorkQueue.WorkQueue;

/**
 *
 * @author yibin
 */
public class Adoption {
     private String animalname;
    private String name;
    private String email;
    
    private String phonenumber;
    private String organization;
    private String statue;

    

    
    
    public Adoption(){
        this.animalname = animalname;
        this.name = name;
        this.email = email;
        this.phonenumber = phonenumber;
        this.organization = organization;
        this.statue = statue;
      
    }

    public String getStatue() {
        return statue;
    }

    public void setStatue(String statue) {
        this.statue = statue;
    }

    
    
    public String getOrganization() {
        return organization;
    }

    public void setOrganization(String organization) {
        this.organization = organization;
    }

    public String getAnimalname() {
        return animalname;
    }

    public void setAnimalname(String animalname) {
        this.animalname = animalname;
    }
       

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhonenumber() {
        return phonenumber;
    }

    public void setPhonenumber(String phonenumber) {
        this.phonenumber = phonenumber;
    }
    
    
    
    @Override
    public String toString() {
        return name;
    }

}