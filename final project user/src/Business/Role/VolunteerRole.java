/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Role;

import Business.Adoption.AdoptionDirectory;
import Business.Animals.AnimalDirectory;
import Business.Enterprise.Enterprise;
import Business.Organization.Organization;
import Business.UserAccount.UserAccount;
import Business.Volunteer.VolunteerDirectory;
import Business.business.AnimalSystem;
import Business.workerrequesttodoctor.WorkerRequestDirectory;
import javax.swing.JPanel;
import userinterface.VolunteerRole.VolunteerPageJPanle;
import userinterface.WorkerRole.WorkerWorkAreaJPanel;

/**
 *
 * @author yibin
 */
public class VolunteerRole extends Role{
 
    @Override
    public JPanel createWorkArea(JPanel userProcessContainer, UserAccount account, Organization organization, Enterprise enterprise, AnimalSystem business, AnimalDirectory animalDirectory, VolunteerDirectory volunteerDirectory, AdoptionDirectory adoptionDirectory, WorkerRequestDirectory workerRequestDirectory) {
        return new VolunteerPageJPanle(userProcessContainer, account, organization, business);
    }
}

