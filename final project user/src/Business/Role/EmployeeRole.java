/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Role;

import Business.Adoption.AdoptionDirectory;
import Business.Volunteer.VolunteerDirectory;
import Business.Animals.AnimalDirectory;
import Business.Enterprise.Enterprise;
import Business.Organization.DoctorOrganization;
import Business.Organization.EmployeeOrganization;
import Business.Organization.Organization;
import Business.UserAccount.UserAccount;
import Business.business.AnimalSystem;
import Business.workerrequesttodoctor.WorkerRequestDirectory;
import javax.swing.JPanel;
import userinterface.DoctorRole.DoctorWorkAreaJPanel;
import userinterface.EmployeeRole.EmployeeWorkAreaJPanel;
import userinterface.LabAssistantRole.LabAssistantWorkAreaJPanel;

/**
 *
 * @author yibin
 */
public class EmployeeRole extends Role {
    @Override
    public JPanel createWorkArea(JPanel userProcessContainer, UserAccount account, Organization organization, Enterprise enterprise, AnimalSystem business,AnimalDirectory animalDirectory,VolunteerDirectory volunteerDirectory,AdoptionDirectory adoptionDirectory,WorkerRequestDirectory workerRequestDirectory) {
        return new EmployeeWorkAreaJPanel(userProcessContainer, account, (EmployeeOrganization) organization, enterprise, animalDirectory);
    }
}
