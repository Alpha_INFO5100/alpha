/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Role;

import Business.Adoption.AdoptionDirectory;
import Business.Volunteer.VolunteerDirectory;
import Business.Animals.AnimalDirectory;
import Business.business.AnimalSystem;
import Business.Enterprise.Enterprise;
import Business.Organization.Organization;
import Business.UserAccount.UserAccount;
import Business.workerrequesttodoctor.WorkerRequestDirectory;
import userinterface.SystemAdminWorkArea.SystemAdminWorkAreaJPanel;
import javax.swing.JPanel;

/**
 *
 * @author raunak
 */
public class SystemAdminRole extends Role{

    @Override
    public JPanel createWorkArea(JPanel userProcessContainer, UserAccount account, Organization organization, Enterprise enterprise, AnimalSystem system,AnimalDirectory animalDirectory,VolunteerDirectory volunteerDirectory,AdoptionDirectory adoptionDirectory,WorkerRequestDirectory workerRequestDirectory) {
        return new SystemAdminWorkAreaJPanel(userProcessContainer, system);
    }
    
}
