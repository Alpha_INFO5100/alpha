/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Role;

import Business.Adoption.AdoptionDirectory;
import Business.Volunteer.VolunteerDirectory;
import Business.Animals.AnimalDirectory;
import Business.business.AnimalSystem;
import Business.Enterprise.Enterprise;
import Business.Organization.Organization;
import Business.UserAccount.UserAccount;
import Business.workerrequesttodoctor.WorkerRequestDirectory;
import java.awt.Component;
import javax.swing.JPanel;

/**
 *
 * @author raunak
 */
public abstract class Role {

    
    
    public enum RoleType{
        Admin("Admin"),
        Doctor("Doctor"),
        LabAssistant("Lab Assistant"),
        Employee("Employee"),
        Worker("Worker");
        
        private String value;
        private RoleType(String value){
            this.value = value;
        }

        public String getValue() {
            return value;
        }

        @Override
        public String toString() {
            return value;
        }
    }
    
    public abstract JPanel createWorkArea(JPanel userProcessContainer, 
            UserAccount account, 
            Organization organization, 
            Enterprise enterprise, 
            AnimalSystem business,
            AnimalDirectory animalDirectory,
            VolunteerDirectory volunteerDirectory,
            AdoptionDirectory adoptionDirectory,
            WorkerRequestDirectory workerRequestDirectory);

    @Override
    public String toString() {
        return this.getClass().getName();
    }
    
    
}