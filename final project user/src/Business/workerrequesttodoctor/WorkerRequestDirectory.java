/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.workerrequesttodoctor;

import Business.Adoption.*;
import Business.Volunteer.*;
import Business.Animals.*;
import Business.UserAccount.*;
import Business.Employee.Employee;
import Business.Role.Role;
import java.util.ArrayList;

/**
 *
 * @author yibin
 */
public class WorkerRequestDirectory {
      private ArrayList<WorkerRequest> workerRequestlist;

    public WorkerRequestDirectory() {
        workerRequestlist = new ArrayList();
    }

    public ArrayList<WorkerRequest> getAccountList() {
        return workerRequestlist;
    }

    public void setAccountList(ArrayList<WorkerRequest> workerRequestlist) {
        this.workerRequestlist = workerRequestlist;
    }

    public WorkerRequest addAccount(){
        WorkerRequest workerRequest = new WorkerRequest();
        
        workerRequestlist.add(workerRequest);
        return workerRequest;
    }
    
    public void deleteAccount(WorkerRequest workerRequest){
        workerRequestlist.remove(workerRequest);
    }
    
    public WorkerRequest searchAccount (String name){
        for(WorkerRequest workerRequest:workerRequestlist){
            if(workerRequest.getName().equals(name)){
                return workerRequest;
            }
        }
        return null;
    }
    
    
}
