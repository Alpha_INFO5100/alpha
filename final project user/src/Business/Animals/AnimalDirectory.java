/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Animals;

import Business.UserAccount.*;
import Business.Employee.Employee;
import Business.Role.Role;
import java.util.ArrayList;

/**
 *
 * @author yibin
 */
public class AnimalDirectory {
      private ArrayList<Animal> animallist;

    public AnimalDirectory() {
        animallist = new ArrayList();
    }

    public ArrayList<Animal> getAccountList() {
        return animallist;
    }

    public void setAccountList(ArrayList<Animal> animallist) {
        this.animallist = animallist;
    }

    public Animal addAccount(){
        Animal animal = new Animal();
        
        animallist.add(animal);
        return animal;
    }
    
    public void deleteAccount(Animal animal){
        animallist.remove(animal);
    }
    
    public Animal searchAccount (String name){
        for(Animal animal:animallist){
            if(animal.getName().equals(name)){
                return animal;
            }
        }
        return null;
    }
    
    
}
