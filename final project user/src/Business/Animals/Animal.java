/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Animals;

import Business.UserAccount.*;
import Business.Employee.Employee;
import Business.Role.Role;
import Business.WorkQueue.WorkQueue;

/**
 *
 * @author yibin
 */
public class Animal {
     private String id;
    private String name;
    private String organization;
    
    private String intro;
    private String catchby;

    
    
    public Animal(){
        this.id = id;
        this.name = name;
        this.organization = organization;
        this.intro = intro;
        this.catchby = catchby;
    }
    
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getOrganization() {
        return organization;
    }

    public void setOrganization(String organization) {
        this.organization = organization;
    }

    public String getIntro() {
        return intro;
    }

    public void setIntro(String intro) {
        this.intro = intro;
    }
    
    public String getCatchby() {
        return catchby;
    }

    public void setCatchby(String catchby) {
        this.catchby = catchby;
    }
    
    @Override
    public String toString() {
        return name;
    }

}