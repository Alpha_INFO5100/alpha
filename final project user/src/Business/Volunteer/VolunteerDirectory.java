/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Volunteer;

import Business.Animals.*;
import Business.UserAccount.*;
import Business.Employee.Employee;
import Business.Role.Role;
import java.util.ArrayList;

/**
 *
 * @author yibin
 */
public class VolunteerDirectory {
      private ArrayList<Volunteer> adoptionlist;

    public VolunteerDirectory() {
        adoptionlist = new ArrayList();
    }

    public ArrayList<Volunteer> getAccountList() {
        return adoptionlist;
    }

    public void setAccountList(ArrayList<Volunteer> animallist) {
        this.adoptionlist = animallist;
    }

    public Volunteer addAccount(){
        Volunteer adoption = new Volunteer();
        
        adoptionlist.add(adoption);
        return adoption;
    }
    
    public void deleteAccount(Volunteer adoption){
        adoptionlist.remove(adoption);
    }
    
    public Volunteer searchAccount (String name){
        for(Volunteer adoption:adoptionlist){
            if(adoption.getName().equals(name)){
                return adoption;
            }
        }
        return null;
    }
    
    
}
