/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package my.petgui;

import java.awt.Color;
import java.awt.Component;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import javax.swing.AbstractCellEditor;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JRadioButton;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.table.TableCellEditor;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;


/**
 *
 * @author zwh
 */
public class PetShowUI extends javax.swing.JFrame {
    private JButton viewButton = new JButton("More details");
    private TheModel model = null;
    private AnimalDetail aui = null;
    private ArrayList<Animal> list;
    /**
     * Creates new form PetShowUI
     */
    public PetShowUI() {
        initComponents();
        setLocationRelativeTo(null);
        list = ReadFile.to_animal_array();
        populateJTable(list);
        this.setDefaultCloseOperation(this.DISPOSE_ON_CLOSE);
    }
    
    //populate the Table
    public void populateJTable(ArrayList<Animal> list){
      
        String[] columnName = {"ID","Name","Organization","Image","Look for more details"};
        Object[][] rows = new Object[list.size()][5];
        for(int i=0; i< list.size(); i++){
            rows[i][0] = list.get(i).getId();
            rows[i][1] = list.get(i).getName();
            rows[i][2] = list.get(i).getOrganization();
            if(list.get(i).getImage() != null){
                ImageIcon image = new ImageIcon(new ImageIcon(list.get(i).getImage())
                        .getImage().getScaledInstance(180, 110, Image.SCALE_SMOOTH));
               rows[i][3] = image;
            }else{
                rows[i][3] = null;
            }
            rows[i][4] = null;
        }
        model = new TheModel(rows, columnName);
        jTable1.setModel(model);
        jTable1.setRowHeight(120);
        
        //set the Button
        ActionPanelEditorRenderer er = new ActionPanelEditorRenderer();
        TableColumn column = jTable1.getColumnModel().getColumn(4);
        column.setCellRenderer(er);
        column.setCellEditor(er);
        jTable1.getColumnModel().getColumn(3).setPreferredWidth(150);
        
        //hide the id column
        TableColumn tc = jTable1.getColumnModel().getColumn(0); 
        tc.setMinWidth(0);   
        tc.setMaxWidth(0);
    }
    
    //Event Controlling of the jButton in the cell 
    class ActionPanelEditorRenderer extends AbstractCellEditor implements TableCellRenderer, TableCellEditor{
        
        JPanel panel_button = new JPanel();
        public ActionPanelEditorRenderer(){
            super();
            
            panel_button.add(viewButton);
            viewButton.setBackground(Color.cyan);
            
            viewButton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent arg0) {
                        // TODO Auto-generated method stub

                        int i = jTable1.getSelectedRow();
                        String s = (String)model.getValueAt(i, 0);

                        //JOptionPane.showMessageDialog(null, s);
                        if(aui==null || aui.getId()!=s){
                            aui = new AnimalDetail(s);
//                            System.out.println("5");
                            aui.setVisible(true);
                            aui.setLocationRelativeTo(null);
                        }
                }
                
            });
        }       
        
        //belows are the overiade methods
        @Override
        public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus,
                        int row, int column) {
            panel_button.setBackground(isSelected ? table.getSelectionBackground() : table.getBackground());
            panel_button.setBackground(isSelected ? table.getSelectionBackground() : table.getBackground());

            return panel_button;
        }

        @Override
        public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected, int row, int column) {
            panel_button.setBackground(table.getSelectionBackground());
            return panel_button;
        }

        @Override
        public Object getCellEditorValue() {
            return null;
        }   
    }
    
    
    //combinational search.search method according to the name and selectedindex of ComboBox
    public  ArrayList<Animal> search(String name, int org_index){
    ArrayList<Animal> list_copy = new ArrayList<Animal>();
    String[] org_list= {"North Shore Animal League","ASPCA","Petfinder","PetSmart","MaddiesFund"};
    if(org_index!=0){
        for(int i=0;i<this.list.size();i++){
            if(list.get(i).getOrganization().equals(org_list[org_index-1]) && list.get(i).getName().toLowerCase()
                    .indexOf(name.toLowerCase())!=-1){
                Animal a = list.get(i);
                list_copy.add(a);
            }
        }
        return list_copy;
    }
    else{
        for(int i=0;i<list.size();i++){
            if(list.get(i).getName().toLowerCase().indexOf(name.toLowerCase())!=-1){
                Animal a = list.get(i);
                list_copy.add(a);
            }
        }
        return list_copy;       
    }
}
    
    
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        buttonGroup1 = new javax.swing.ButtonGroup();
        jPanel1 = new javax.swing.JPanel();
        dogButton = new javax.swing.JRadioButton();
        catButton = new javax.swing.JRadioButton();
        smallButton = new javax.swing.JRadioButton();
        birdButton = new javax.swing.JRadioButton();
        barnyardButton = new javax.swing.JRadioButton();
        jLabel1 = new javax.swing.JLabel();
        jPanel2 = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        jTextField1 = new javax.swing.JTextField();
        jButton1 = new javax.swing.JButton();
        jComboBox1 = new javax.swing.JComboBox();
        jButton2 = new javax.swing.JButton();
        jPanel3 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jPanel1.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));

        buttonGroup1.add(dogButton);
        dogButton.setText("Dogs");
        dogButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                dogButtonActionPerformed(evt);
            }
        });

        buttonGroup1.add(catButton);
        catButton.setText("Cats");
        catButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                catButtonActionPerformed(evt);
            }
        });

        buttonGroup1.add(smallButton);
        smallButton.setText("Small&Funny");
        smallButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                smallButtonActionPerformed(evt);
            }
        });

        buttonGroup1.add(birdButton);
        birdButton.setText("Birds");
        birdButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                birdButtonActionPerformed(evt);
            }
        });

        buttonGroup1.add(barnyardButton);
        barnyardButton.setText("Barnyard");
        barnyardButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                barnyardButtonActionPerformed(evt);
            }
        });

        jLabel1.setFont(new java.awt.Font("宋体", 1, 12)); // NOI18N
        jLabel1.setText("Search By Type ");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(dogButton)
                    .addComponent(birdButton)
                    .addComponent(barnyardButton)
                    .addComponent(smallButton)
                    .addComponent(catButton)
                    .addComponent(jLabel1))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addComponent(jLabel1)
                .addGap(18, 18, 18)
                .addComponent(catButton)
                .addGap(18, 18, 18)
                .addComponent(dogButton)
                .addGap(18, 18, 18)
                .addComponent(birdButton)
                .addGap(18, 18, 18)
                .addComponent(smallButton, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(barnyardButton)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel2.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));

        jLabel2.setFont(new java.awt.Font("宋体", 3, 18)); // NOI18N
        jLabel2.setText("Adopt Your PET!");

        jButton1.setText("Search by Name");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        jComboBox1.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Choose the Organization ", "North Shore Animal League", "ASPCA", "Petfinder", "PetSmart", "MaddiesFund" }));
        jComboBox1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jComboBox1ActionPerformed(evt);
            }
        });

        jButton2.setText("Get back");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel2)
                    .addComponent(jButton2))
                .addGap(210, 210, 210)
                .addComponent(jTextField1, javax.swing.GroupLayout.PREFERRED_SIZE, 129, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jButton1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jComboBox1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(89, 89, 89))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addContainerGap(43, Short.MAX_VALUE)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jTextField1, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton1)
                    .addComponent(jComboBox1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(33, 33, 33))
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(20, 20, 20)
                .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jButton2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        jPanel3.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));

        jTable1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane1.setViewportView(jTable1);

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 861, Short.MAX_VALUE)
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 510, Short.MAX_VALUE)
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void smallButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_smallButtonActionPerformed
        // TODO add your handling code here:
        list = ReadFile.find_class_animal("small");
        this.populateJTable(list);
        jTextField1.setText("");
        jComboBox1.setSelectedIndex(0);
    }//GEN-LAST:event_smallButtonActionPerformed

    private void catButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_catButtonActionPerformed
        // TODO add your handling code here:
        list = ReadFile.find_class_animal("cat");
        this.populateJTable(list);
        jTextField1.setText("");
        jComboBox1.setSelectedIndex(0);
    }//GEN-LAST:event_catButtonActionPerformed

    private void dogButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_dogButtonActionPerformed
        // TODO add your handling code here:
        list = ReadFile.find_class_animal("dog");
        this.populateJTable(list);
        jTextField1.setText("");
        jComboBox1.setSelectedIndex(0);
    }//GEN-LAST:event_dogButtonActionPerformed

    private void birdButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_birdButtonActionPerformed
        // TODO add your handling code here:
        list = ReadFile.find_class_animal("bird");
        this.populateJTable(list);
        jTextField1.setText("");      
        jComboBox1.setSelectedIndex(0);
    }//GEN-LAST:event_birdButtonActionPerformed

    private void barnyardButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_barnyardButtonActionPerformed
        // TODO add your handling code here:
        list = ReadFile.find_class_animal("barnyard");
        this.populateJTable(list);
        jTextField1.setText("");
        jComboBox1.setSelectedIndex(0);
    }//GEN-LAST:event_barnyardButtonActionPerformed

    private void jComboBox1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jComboBox1ActionPerformed
        // TODO add your handling code here:
        //System.out.println(jComboBox1.getSelectedIndex());
        
        if(catButton.isSelected()){
            list = ReadFile.find_class_animal("cat");
        }
        else if(dogButton.isSelected()){
            list = ReadFile.find_class_animal("dog");
        }
        else if(birdButton.isSelected()){
            list = ReadFile.find_class_animal("bird");
        }
        else if(smallButton.isSelected()){
            list = ReadFile.find_class_animal("small");
        }
        else if(barnyardButton.isSelected()){
            list = ReadFile.find_class_animal("barnyard");
        }
        else{
            list = ReadFile.to_animal_array();
        }
        
        
        
        
        if(jTextField1.getText().trim().length()==0){
            list = this.search("",jComboBox1.getSelectedIndex());
            this.populateJTable(list);
        }
        else{
            list = this.search(jTextField1.getText(), jComboBox1.getSelectedIndex());
            this.populateJTable(list);
        }
    }//GEN-LAST:event_jComboBox1ActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        // TODO add your handling code here:
        if(jTextField1.getText().trim().length()!=0){
            list = this.search(jTextField1.getText(),jComboBox1.getSelectedIndex());
            this.populateJTable(list);
        }else{
            
            if(catButton.isSelected()){
                list = ReadFile.find_class_animal("cat");
            }
            else if(dogButton.isSelected()){
                list = ReadFile.find_class_animal("dog");
            }
            else if(birdButton.isSelected()){
                list = ReadFile.find_class_animal("bird");
            }
            else if(smallButton.isSelected()){
                list = ReadFile.find_class_animal("small");
            }
            else if(barnyardButton.isSelected()){
                list = ReadFile.find_class_animal("barnyard");
            }
            else{
                list = ReadFile.to_animal_array();
            }
            
            list = this.search("",jComboBox1.getSelectedIndex());
            this.populateJTable(list);
            
            
        }
    }//GEN-LAST:event_jButton1ActionPerformed

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        // TODO add your handling code here:
        WelcomeUI wui = new WelcomeUI();
        wui.setVisible(true);
        this.dispose();
        
    }//GEN-LAST:event_jButton2ActionPerformed
    

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(PetShowUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(PetShowUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(PetShowUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(PetShowUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new PetShowUI().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JRadioButton barnyardButton;
    private javax.swing.JRadioButton birdButton;
    private javax.swing.ButtonGroup buttonGroup1;
    private javax.swing.JRadioButton catButton;
    private javax.swing.JRadioButton dogButton;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JComboBox jComboBox1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable jTable1;
    private javax.swing.JTextField jTextField1;
    private javax.swing.JRadioButton smallButton;
    // End of variables declaration//GEN-END:variables
}
