/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package my.petgui;

/**
 *
 * @author zwh
 */
public class Animal {
    private String id;
    private String name;
    private String organization;
    private byte[] Image;
    private String intro;
    
    public Animal(String id, String name, String organization, byte[] image ,String intro){
        this.id = id;
        this.name = name;
        this.organization = organization;
        this.Image = image;
        this.intro = intro;
    }
    
    public String getId(){
        return id;
    }
    
    public String getName(){
        return name;
    }
    
    public String getOrganization(){
        return organization;
    }
    
    public byte[] getImage(){
        return Image;
    }
    
    public String getIntro(){
        return intro;
    }
    
    public void setId(String id){
        this.id = id;
    }
    
    public void setName(String name){
        this.name = name;
    }

    public void setImage(byte[] image){
        this.Image = image;
    }
    
    public void setOrganization(String org){
        this.organization = org;
    }
    
    public void setIntro(String intro){
        this.intro = intro;
    }    
}
