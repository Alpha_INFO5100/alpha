/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package my.petgui;
import java.io.BufferedReader;
import org.json.*;
//import net.sf.json.JSONArray;
//import net.sf.json.JSONObject;
import java.util.ArrayList;

//import java.awt.image.BufferedImage;
//import java.io.ByteArrayInputStream;
import javax.imageio.stream.FileImageInputStream;
import java.io.ByteArrayOutputStream;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.FileNotFoundException;
import java.io.InputStreamReader;


/**
 *
 * @author zwh
 */
public class ReadFile {
    
//transform a image to byte[]
public static byte[] imageToByte(String imgPath){
    byte[] data = null;
    FileImageInputStream input = null;
    try {
        input = new FileImageInputStream(new File(imgPath));
        ByteArrayOutputStream output = new ByteArrayOutputStream();
        byte[] imageByte = new byte[1024];
        int numBytesRead = 0;
        while ((numBytesRead = input.read(imageByte)) != -1) {
            output.write(imageByte, 0, numBytesRead);
     }
        data = output.toByteArray();
        output.close();
        input.close();
   }
   catch (FileNotFoundException ex1) {
       ex1.printStackTrace();
   }
   catch (IOException ex1) {
        ex1.printStackTrace();
   }
   return data;
 }

// return an ArrayList<Animal> of all animals objects
public static ArrayList<Animal> to_animal_array(){
    ArrayList<Animal> list = new ArrayList<Animal>();
    String lineTxt="";
    //read the file to string
    try {
        File file = new File("src/my/petgui/json_data/animals.json");
        if (file.isFile() && file.exists()) {
            InputStreamReader read = new InputStreamReader(new FileInputStream(file), "UTF-8");
            BufferedReader bufferedReader = new BufferedReader(read);
            lineTxt = bufferedReader.readLine();

        }
        else{
            System.out.print("hfdf");
        }
    } catch (IOException e) {
        System.out.println("Error reading file content!");
        e.printStackTrace();
    }
//    System.out.print(lineTxt)
    JSONArray jsonarray = new JSONArray(lineTxt);
    for(int i=0;i<jsonarray.length();i++){
        Animal a;
        JSONObject jsonobject = jsonarray.getJSONObject(i);
        String id = jsonobject.getString("id");
        String name = jsonobject.getString("name");
        String image = jsonobject.getString("image");
        String org = jsonobject.getString("org");
        String intro = jsonobject.getString("intro");
        byte[] image_by = imageToByte("src/my/petgui/anmials_image/"+image);
        
//        System.out.println(image_by.length);
        a = new Animal(id,name,org,image_by,intro);
        list.add(a);
        
    }
    
    return list;
    
    
}

//return a animal that fits for the id
public static Animal find_animal(String strid){
    Animal a = null; 
    ArrayList<Animal> list = new ArrayList<Animal>();
    list = to_animal_array();
    for(int i=0;i<list.size();i++){
        String id = list.get(i).getId();
        if(id.equals(strid)){
            //System.out.println(strid);
            a = list.get(i);
        }
        else{

        }
    }
    return a;
    
}

//find animals according to the class such as cat
public static ArrayList<Animal> find_class_animal(String category){
    ArrayList<Animal> list_categ = new ArrayList<Animal>();
    ArrayList<Animal> list = to_animal_array();
//    System.out.println(list.size());
    for(int i=0;i<list.size();i++){
        //System.out.println(list.get(i).getId().substring(0,list.get(i).getId().length()-1));
        if(list.get(i).getId().substring(0, list.get(i).getId().length()-1).equals(category)){
//            System.out.print("hhh");
            Animal a = list.get(i);
            list_categ.add(a);
        }
    }
    return list_categ;
}

public static void main(String[] args){
    
}
    
}
