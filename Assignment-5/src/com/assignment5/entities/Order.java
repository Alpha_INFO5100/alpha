/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.assignment5.entities;

/**
 *
 * @author kasai
 */
public class Order {
    
    private int orderId;
    private Customer customer;
    private SalesPerson saler;
    private Item item;
    private String market;

    public Order(int orderId, SalesPerson saler, Customer customer, Item item, String market) {
        this.orderId = orderId;
        this.saler = saler;
        this.customer = customer;
        this.item = item;
        this.market = market;
    }

    public int getOrderId() {
        return orderId;
    }

    public void setOrderId(int orderId) {
        this.orderId = orderId;
    }

    public SalesPerson getSalesPerson() {
        return saler;
    }

    public void setSalesPerson(SalesPerson saler) {
        this.saler = saler;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customerId) {
        this.customer = customerId;
    }

    public Item getItem() {
        return item;
    }

    public void setItem(Item item) {
        this.item = item;
    }
    
    public String getMarket() {
        return this.market;
    }
    
    public void setMarket(String market) {
        this.market = market;
    }
}
