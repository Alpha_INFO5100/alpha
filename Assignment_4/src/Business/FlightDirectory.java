package Business;
import java.util.ArrayList;


public class FlightDirectory {
    private ArrayList<Flight> fld;
    private String airlineName;
    
    public String getAirlineName() {
		return airlineName;
	}
	public void setAirlineName(String airlineName) {
		this.airlineName = airlineName;
	}
	public FlightDirectory(){
        ArrayList<Flight> fld;
    	this.fld = new ArrayList<Flight>();
    }
	public ArrayList<Flight> getFld() {
		return fld;
	}
            public void setFld(ArrayList<Flight> fld) {
		this.fld = fld;
	}
            
	public Flight addFlight(){
		Flight fl = new Flight();
		fld.add(fl);
		return fl;
	}
        public void deleteFlight(Flight fl){
            fld.remove(fl);
        }
    
}
