/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

/**
 *
 * @author liaoz
 */
public class Customer {
    String CusName;
    String CusFlight;
    String CusSeatNo;

    public String getCusName() {
        return CusName;
    }

    public void setCusName(String CusName) {
        this.CusName = CusName;
    }

    public String getCusFlight() {
        return CusFlight;
    }

    public void setCusFlight(String CusFlight) {
        this.CusFlight = CusFlight;
    }

    public String getCusSeatNo() {
        return CusSeatNo;
    }

    public void setCusSeatNo(String CusSeatNo) {
        this.CusSeatNo = CusSeatNo;
    }
    
    @Override
    public String toString() {
        return  this.getCusName();
    }
}
